from django.urls import path
from . import views

urlpatterns = [
    path('mebel/', views.mebel, name='mebel'),
    path('mebels/', views.mebels, name='mebels'),
]